<?php
    $DB_NAME = "razor_comp";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT id_merk, nama_merk FROM merk ORDER BY nama_merk ASC";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $nama_merk = array();
            while($nm_merk = mysqli_fetch_assoc($result)){
                array_push($nama_merk, $nm_merk);
            }
            echo json_encode($nama_merk);
        }
    }
?>
