<?php
    class database{
        public $host = "localhost",
        $uname = "root",
        $pass = "",
        $db = "razor_comp",
        $con,
        $path = "images/";
        

        public function __construct()
        {   
            $this->con = mysqli_connect($this->host,$this->uname,$this->pass,$this->db);
            date_default_timezone_set('Asia/Jakarta');
        }

        public function tampil_data_produk(){
            $sql = "SELECT p.id_produk, m.nama_merk, p.nama_produk, CAST(p.harga as varchar(10)) as harga, k.nama_kategori, p.linkyt, 
                concat('http://localhost/tokomasker/images/',foto) as url
                FROM produk p, merk m, kategori k
                WHERE p.id_merk = m.id_merk AND p.id_kategori = k.id_kategori";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function tampil_data_merk(){
            $sql = "SELECT id_merk, nama_merk
                FROM merk";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function tampil_data_kategori(){
            $sql = "SELECT id_kategori, nama_kategori
                FROM kategori";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }
    }
?>