<?php
    $DB_NAME = "razor_comp";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $mode = $_POST['mode'];
        $respon = array();
        $respon['kode'] = '000';

        switch($mode){
            case "insert":
                $id_produk = $_POST["id_produk"];
                $nama_produk = mysqli_real_escape_string($conn,trim($_POST["nama_produk"]));
                $nama_merk = $_POST["nama_merk"];
                $harga = $_POST["harga"];
				$nama_kategori = $_POST["nama_kategori"];
				$linkyt = $_POST["linkyt"];
                $imstr = $_POST["image"];
                $file = $_POST["file"];
                $path = "images/";

                $sql = "SELECT id_merk FROM merk WHERE nama_merk='$nama_merk'";
                $sql1 = "SELECT id_kategori FROM kategori WHERE nama_kategori='$nama_kategori'";

                $result = mysqli_query($conn,$sql);

                $result1 = mysqli_query($conn,$sql1);

                if (mysqli_num_rows($result)>0 && mysqli_num_rows($result1)>0) {
                    $data = mysqli_fetch_assoc($result);
                    $data1 = mysqli_fetch_assoc($result1);
                    $id_merk = $data['id_merk'];
                    $id_kategori = $data1['id_kategori'];

                    $sql = "INSERT INTO produk(id_produk, id_merk, nama_produk, harga, id_kategori, linkyt, foto) VALUES(
                        '$id_produk','$id_merk','$nama_produk', CAST('$harga' as int), '$id_kategori', '$linkyt', '$file')";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                            $sql = "DELETE FROM produk WHERE id_produk='$id_produk'";
                            mysqli_query($conn,$sql);
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();    
                        }else{
                            echo json_encode($respon); exit(); //insert data sukses semua
                        }
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }
            break;

            case "update":
                $id_produk = $_POST["id_produk"];
                $nama_produk = mysqli_real_escape_string($conn,trim($_POST["nama_produk"]));
                $nama_merk = $_POST["nama_merk"];
                $harga = (int)$_POST["harga"];
				$nama_kategori = $_POST["nama_kategori"];
				$linkyt = $_POST["linkyt"];
                $imstr = $_POST["image"];
                $file = $_POST["file"];
                $path = "images/";

                $sql = "SELECT id_merk FROM merk WHERE nama_merk='$nama_merk'";
                $sql1 = "SELECT id_kategori FROM kategori WHERE nama_kategori='$nama_kategori'";

                $result = mysqli_query($conn,$sql);
                $result1 = mysqli_query($conn,$sql1);

                if (mysqli_num_rows($result)>0 && mysqli_num_rows($result1)>0) {
                    $data = mysqli_fetch_assoc($result);
                    $data1 = mysqli_fetch_assoc($result1);
                    $id_merk = $data['id_merk'];
                    $id_kategori = $data1['id_kategori'];

                    $sql = "";
                    if($imstr==""){
                        $sql = "UPDATE produk SET id_merk='$id_merk', nama_produk='$nama_produk', harga='$harga', id_kategori='$id_kategori', linkyt='$linkyt'
                        where id_produk='$id_produk'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon); exit();
                        }else{
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();
                        }
                    }else{
                        if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();    
                        }else{
                            $sql = "UPDATE produk SET id_merk='$id_merk', nama_produk='$nama_produk', harga='$harga', id_kategori='$id_kategori', linkyt='$linkyt', foto='$file'
                            where id_produk='$id_produk'";
                            $result = mysqli_query($conn,$sql);
                            if($result){
                                echo json_encode($respon); exit(); //update data sukses semua
                            }else{
                                $respon['kode'] = "111";
                                echo json_encode($respon); exit();
                            }
                        }
                    }
                }
            break;

            case "delete":
                $id_produk = $_POST["id_produk"];
                $sql = "SELECT foto from produk where id_produk='$id_produk'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(mysqli_num_rows($result)>0){
                        $data = mysqli_fetch_assoc($result);
                        $foto = $data['foto'];
                        $path = "images/";
                        unlink($path.$foto);
                    }
                    $sql = "DELETE from produk where id_produk='$id_produk'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon); exit(); //delete data sukses
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }
            break;
        }
    }
?>