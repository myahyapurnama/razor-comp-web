<?php
    $DB_NAME = "razor_comp";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $nama = mysqli_real_escape_string($conn,trim($_POST["nama_produk"]));
        $sql = "SELECT p.id_produk, m.nama_merk, p.nama_produk, CAST(p.harga as varchar(10)) as harga, k.nama_kategori, p.linkyt, 
            concat('http://192.168.1.5/razor-comp-web/images/', p.foto) as url
            FROM produk p, merk m, kategori k
            WHERE p.id_merk = m.id_merk AND p.id_kategori = k.id_kategori AND p.nama_produk like '%$nama%'";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_produk = array();
            while($produk = mysqli_fetch_assoc($result)){
                array_push($data_produk,$produk);
            }
            echo json_encode($data_produk);
        }
    }
?>